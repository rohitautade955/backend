const express = require('express')
const router = express.Router()
const Intern = require('./models/intern')

router.get('/',async(req,res)=>{
    try{
        const interns=  await Intern.find()
        res.json(interns)


    }catch(err)
    {
        res.send("Eror"+err)
    }
    

})
router.get('/:id',async(req,res)=>{
    try{
        const intern=  await Intern.findById(req.params.id)
        res.json(intern)


    }catch(err)
    {
        res.send("Eror"+err)
    }
    

})
router.post('/',async(req,res)=>{
    const intern = new Intern({
        name: req.body.name,
        college: req.body.college
    })
    try{
       const a1= await intern.save()
        res.json(a1)

    }catch(err)
    {

        res.send('error')

    }
})
router.patch('/:id',async(req,res)=>{
    try{
const intern = await Intern.findById(req.params.id)
intern.college=req.body.college
const a1=await intern.save()
res.json(a1)
    }catch(err)
    {
        res.send("error")

    }
})
router.delete('/:id',async(req,res)=>{
    try{
const intern = await Intern.findById(req.params.id)
const a1=await intern.remove()
res.json(a1)
    }catch(err)
    {
        res.send("delete")

    }
})

module.exports =  router 