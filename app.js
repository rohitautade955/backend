const express = require('express')
const mongoose= require('mongoose')
const url = 'mongodb://localhost/rohit'
const app = express()
mongoose.connect(url,{useNewUrlParser:true}) 
const con = mongoose.connection

con.on('open',()=>{
console.log("Connected")
})
// app.get('/',(req,res)=>{
//     res.send(' welcome rohit')
// })
app.use(express.json())


const internsRouter = require('./routes/interns')
app.use('/interns',internsRouter)

app.listen(2020,()=>{
    console.log("server started")
})
